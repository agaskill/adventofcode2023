const fs = require('fs')

const lines = fs
	.readFileSync(process.argv[2], { encoding: 'utf-8' })
	.split('\n')

const lookup = [
	'zero',
	'one',
	'two',
	'three',
	'four',
	'five',
	'six',
	'seven',
	'eight',
	'nine']
function convert(num) {
	const value = lookup.indexOf(num)
	if (value > 0) return value
	return num
}

console.log(lines.reduce(function (sum, line) {
	if (!line.length) return sum
	console.log("checking line " + line)
	const regex = /\d|one|two|three|four|five|six|seven|eight|nine/
	let m = regex.exec(line)
	const num1 = m[0]
	let num2
	while (m) {
		num2 = m[0]
		line = line.substring(m.index + 1)
		m = regex.exec(line)
	}
	const num = Number(`${convert(num1)}${convert(num2)}`)
	console.log(`Numerals are ${num1} and ${num2}, number is ${num}`)
	return sum + num
}, 0))


