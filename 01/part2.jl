const lookup = [
    "one"
    "two"
    "three"
    "four"
    "five"
    "six"
    "seven"
    "eight"
    "nine"]

function convert(num)
    numeric = findfirst(isequal(num), lookup)
    if isnothing(numeric)
        parse(Int64, num)
    else
        numeric
    end
end

const rxnumbers = r"\d|one|two|three|four|five|six|seven|eight|nine"

function processfile(filename)
    sum([processline(line) for line in eachline(filename)])
end

function processline(line)
    m = match(rxnumbers, line)
    num1 = m.match
    num2 = nothing
    while !isnothing(m)
        num2 = m.match
        line = line[m.offset+1:end]
        m = match(rxnumbers, line)
    end
    convert(num1) * 10 + convert(num2)
end

println(processfile("input.txt"))
