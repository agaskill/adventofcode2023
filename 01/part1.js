const fs = require('fs')

const lines = fs
	.readFileSync(process.argv[2], { encoding: 'utf-8' })
	.split('\n')

console.log(lines.reduce(function (sum, line) {
	if (!line.length) return sum
	console.log("checking line " + line)
	let num1, num2
	for (const m of line.matchAll(/\d/g)) {
		console.log(m)
		if (num1) num2 = m[0]
		else num1 = m[0]
	}
	if (!num2) num2 = num1
	const num = Number(`${num1}${num2}`)
	console.log(`Numerals are ${num1} and ${num2}, number is ${num}`)
	return sum + num
}, 0))
