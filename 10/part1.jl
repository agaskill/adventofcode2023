const souths = "|7F"
const norths = "|JL"

struct Direction
    action
    required
end

function processfile(filename)
    maze = readlines(filename)
    start = findstart(maze)
    println("Start location is ", start)
    r, c = start

    longestloop = 0
    loopstart = nothing

    up = Direction(moveup, "|7F")
    down = Direction(movedown, "|JL")
    left = Direction(moveleft, "-FL")
    right = Direction(moveright, "-7J")
    for direction in [up, down, left, right]
        (next, from) = direction.action(start)
        #println("Trying ", next, " from ", from)
        if !inbounds(maze, next)
            continue
        end
        tile = tileat(maze, next)
        if contains(direction.required, tile)
            looplen = findloop(maze, start, next, from)
            println("Loop in direction ", next, " is length ", looplen)
            if looplen > longestloop
                longestloop = looplen
                loopstart = (next, from)
            end
        end
    end
    println("Farthest point is ", longestloop / 2)
end

inbounds(maze, loc) = 
    (0 < loc[1] <= length(maze)) &&
    (0 < loc[2] <= length(maze[1]))

tileat(maze, loc) = maze[loc[1]][loc[2]]

moveright(pos) = ((pos[1], pos[2] + 1), "W")
moveleft(pos) = ((pos[1], pos[2] - 1), "E")
moveup(pos) = ((pos[1] - 1, pos[2]), "S")
movedown(pos) = ((pos[1] + 1, pos[2]), "N")
const movemap = Dict(
    "|S" => moveup,
    "|N" => movedown,
    "-W" => moveright,
    "-E" => moveleft,
    "LE" => moveup,
    "LN" => moveright,
    "JW" => moveup,
    "JN" => moveleft,
    "7W" => movedown,
    "7S" => moveleft,
    "FE" => movedown,
    "FS" => moveright,
)

function findloop(maze, start, current, from)
    looplen = 1
    while current != start
        looplen += 1
        tile = tileat(maze, current)
        #println("Tile at ", current, " is ", tile, ", from ", from)
        action = movemap[string(tile, from)]
        (current, from) = action(current)
    end
    looplen
end

function findstart(maze)
    for r in eachindex(maze)
        row = maze[r]
        for c in eachindex(row)
            if row[c] == 'S'
            return (r, c)
            end
        end
    end
end

processfile(ARGS[1])