const fs = require("fs")

function processfile(filename) {
    const content = fs.readFileSync(filename).toString()
    const lines = content.split('\n')
    const rlSequence = lines[0]
    console.log(rlSequence)
    const nodes = {}
    let node = getOrCreate(nodes, "AAA")
    for (const line of lines.slice(2)) {
        parseNode(line, nodes)
    }
    let moves = 0
    while (node.label !== 'ZZZ') {
        const dir = rlSequence[moves % rlSequence.length]
        moves++
        node = dir === 'R' ? node.right : node.left
        console.log(`Moved ${dir} to ${node.label}`)
    }
    console.log(`Got to ZZZ in ${moves} moves`)
}

function parseNode(line, nodes) {
    const m = /(\w{3}) = \((\w{3}), (\w{3})\)/.exec(line)
    if (!m)
        throw new Error("Bad format")
    const label = m[1]
    const leftLabel = m[2]
    const rightLabel = m[3]
    const node = getOrCreate(nodes, label)
    node.left = getOrCreate(nodes, leftLabel)
    node.right = getOrCreate(nodes, rightLabel)
    return node
}

function getOrCreate(nodes, label) {
    let node = nodes[label]
    if (!node) {
        nodes[label] = node = {label}
    }
    return node
}

processfile("input.txt")