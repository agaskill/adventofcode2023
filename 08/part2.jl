mutable struct Node
    label::AbstractString
    left::Union{Node,Missing}
    right::Union{Node,Missing}
end

mutable struct GraphNode
    label::AbstractString
    dist::Int64
    next::Union{GraphNode,Missing}
end

function processfile(filename)
    fs = open(filename)
    pattern = readline(fs)
    println(pattern)
    nodes = parsenodes(fs)
    startingNodes = filter(n -> n.label[3] == 'A', nodes)
    println(length(startingNodes), " starting nodes")
    pathGraphs = map(buildPathGraph(pattern), startingNodes)
    if all(isSimpleLoop, pathGraphs)
        println("Simple loop, all paths will enter cycle with same distance cost")
        lcm(map(g -> g.dist, pathGraphs))
    else
        findTotalMovesIteratively(pathGraphs)
    end
end

function isSimpleLoop(path)
    path.next === path.next.next && path.dist == path.next.dist
end

function findTotalMovesIteratively(pathGraphs)
    pathCount = length(pathGraphs)
    starts = collect(pathGraphs)
    remaining = map(g -> g.dist, pathGraphs)
    notAtEnd = pathCount
    totalMoves = 0
    loopCount = 0
    while notAtEnd > 0
        mindist = minimum(remaining)
        totalMoves += mindist
        if (loopCount += 1) % 10000000 == 0
            println(notAtEnd, " paths are not at an end state")
            println(remaining)
            println("Advancing ", mindist, " moves to new total ", totalMoves)
        end
        notAtEnd = pathCount
        for i in 1:pathCount
            if (remaining[i] -= mindist) == 0
                starts[i] = starts[i].next
                remaining[i] = starts[i].dist
                notAtEnd -= 1
            end
        end
    end
    totalMoves
end

function buildPathGraph(pattern)
    node -> buildPathGraph(node, pattern)
end

function buildPathGraph(node, pattern)
    println("Building path graph from ", node.label)
    visited = Dict()
    idx = 1
    patternLength = length(pattern)
    graphRoot = GraphNode(node.label, -1, missing)
    visited[makeVisitedKey(node, 0)] = graphRoot
    graphNode = graphRoot
    looped = false
    while !looped
        dist = 0
        atz = false
        while !atz
            node = pattern[idx] == 'R' ? node.right : node.left
            if (idx += 1) > patternLength
                idx = 1
            end
            dist += 1
            atz = node.label[3] == 'Z'
        end
        println(dist, " moves to node ", node.label)
        graphNode.dist = dist
        visitedKey = makeVisitedKey(node, idx)
        if haskey(visited, visitedKey)
            graphNode.next = visited[visitedKey]
            looped = true
        else
            nextNode = GraphNode(node.label, -1, missing)
            visited[visitedKey] = nextNode
            graphNode.next = nextNode
            graphNode = nextNode
        end
    end
    graphRoot
end

function makeVisitedKey(node, idx)
    string(node.label, "-", idx)
end

function parsenodes(fs)
    nodes = Dict()
    while !eof(fs)
        line = readline(fs)
        if (isempty(line))
            continue
        end
        parsenode(line, nodes)
    end
    collect(values(nodes))
end

nodeline = r"(\w{3}) = \((\w{3}), (\w{3})\)"
function parsenode(line, nodes)
    match
    m = match(nodeline, line)
    if isnothing(m)
        error("Unexpected line format")
    end
    label = m[1]
    leftLabel = m[2]
    rightLabel = m[3]
    node = getorcreate(nodes, label)
    node.left = getorcreate(nodes, leftLabel)
    node.right = getorcreate(nodes, rightLabel)
    node
end

function getorcreate(nodes, label)
    if haskey(nodes, label)
        nodes[label]
    else
        node = Node(label, missing, missing)
        nodes[label] = node
        node
    end
end

println(processfile(ARGS[1]))