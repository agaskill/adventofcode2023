const fs = require("fs")

function processfile(filename) {
    const content = fs.readFileSync(filename).toString()
    const lines = content.split('\n')
    const pattern = lines[0]
    console.log(pattern)
    const nodes = {}
    for (const line of lines.slice(2)) {
        parseNode(line, nodes)
    }
    const rlSequence = Array.prototype.map.call(pattern, dir => dir === 'R' ? moveRight : moveLeft)
    const pathGraphs = Object.values(nodes)
        .filter(node => node.label[2] === 'A')
        .map(node => buildPathGraph(node, rlSequence))
    if (pathGraphs.every(g => g.next.next === g.next && g.dist === g.next.dist)) {
        console.log("Simple case: all paths will enter a loop after their first route with the same distance as the first route, so just find the LCM of the following distances")
        console.log(pathGraphs.map(g => g.dist))
    }
    else {
        iterativelyFindTotalMoves(pathGraphs)
    }
}

function iterativelyFindTotalMoves(pathGraphs) {
    const states = pathGraphs.map(graphNode => ({ graphNode, remaining: graphNode.dist }))
    let notAtEnd = states.length
    let totalMoves = 0
    let loopTimes = 0
    while (notAtEnd > 0) {
        const mindist = minimumRemainingDistance(states)
        totalMoves += mindist
        if (++loopTimes % 10000000 === 0) {
            console.log(`${notAtEnd} paths are not at an end state`)
            console.log(states.map(state => ({node: state.graphNode.label, remaining: state.remaining})))
            console.log(`Advancing ${mindist} moves, total is now ${totalMoves}`)
        }
        notAtEnd = 0
        for (const state of states) {
            state.remaining -= mindist
            if (state.remaining === 0) {
                state.remaining = state.graphNode.dist
                state.graphNode = state.graphNode.next
            } else {
                notAtEnd++
            }
        }
    }
    console.log(`Got to all XXZs in ${totalMoves} moves`)
}

function minimumRemainingDistance(states) {
    let min = Infinity
    for (const state of states) {
        if (state.remaining < min) {
            min = state.remaining
        }
    }
    return min
}

function buildPathGraph(node, pattern) {
    /* From the starting node, follow the pattern until an XXZ node is reached.
     * If that (node,idx) pair has been seen before, link to it.
     * Otherwise, create a new (node,idx) pair and link to it with the move distance.
     */
    console.log("Building path graph starting from node " + node.label)
    const visited = {}
    let idx = 0
    const patternLength = pattern.length
    const graphRoot = makeGraphNode(node, 0)
    visited[makeVisitedKey(node, 0)] = graphRoot
    let graphNode = graphRoot
    while (true) {
        let dist = 0
        do {
            node = pattern[idx](node)
            if (++idx === patternLength) idx = 0
            dist++
        } while (node.label[2] !== 'Z')
        console.log(`${dist} moves to node ${node.label}.  At pattern index ${idx}`)
        graphNode.dist = dist
        const visitedKey = `${node.label}-${idx}`
        const existing = visited[visitedKey]
        if (existing) {
            graphNode.next = existing
            break
        }
        else {
            const nextNode = { label: node.label, idx }
            visited[visitedKey] = nextNode
            graphNode.next = nextNode
            graphNode = nextNode
        }
    }
    console.log(graphRoot)
    return graphRoot
}

function makeVisitedKey(node, idx) {
    return `${node.label}-${idx}`
}

function makeGraphNode(node, idx) {
    return { label: node.label, idx }
}

function moveLeft(node) {
    return node.left
}

function moveRight(node) {
    return node.right
}

function parseNode(line, nodes) {
    const m = /(\w{3}) = \((\w{3}), (\w{3})\)/.exec(line)
    if (!m)
        throw new Error("Bad format")
    const label = m[1]
    const leftLabel = m[2]
    const rightLabel = m[3]
    const node = getOrCreate(nodes, label)
    node.left = getOrCreate(nodes, leftLabel)
    node.right = getOrCreate(nodes, rightLabel)
    return node
}

function getOrCreate(nodes, label) {
    let node = nodes[label]
    if (!node) {
        nodes[label] = node = {label}
    }
    return node
}

//processfile("example-3.txt")
processfile("input.txt")