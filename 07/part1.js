const fs = require("fs")

function processfile(filename) {
    const content = fs.readFileSync(filename).toString()
    const lines = content.split('\n')
    const hands = lines.map(parsehand)
    console.log(hands)
    hands.sort(compareHands)
    let result = 0
    for (let i = 0; i < hands.length; i++) {
        result += (i + 1) * hands[i].bid
    }
    return result
}

function parsehand(line) {
    const [cards, bid] = line.trim().split(' ')
    return  {
        cards,
        type: handType(cards),
        comparable: mapComparableCards(cards),
        bid: Number(bid)
    }
}

function handType(cards) {
    const groups = {}
    for (const card of cards) {
        groups[card] = 1 + (groups[card] || 0)
    }
    const distincts = Object.entries(groups).sort((a, b) => b[1] - a[1])
    switch (distincts.length) {
        case 1:
            // five of a kind
            return 7
        case 2:
            // 4 of a kind or full house
            return distincts[0][1] === 4 ? 6 : 5
        case 3:
            // 3 of a kind or 2 pair
            return distincts[0][1] === 3 ? 4 : 3
        case 4:
            // one pair
            return 2
        case 5:
            // high card
            return 1
        default:
            throw new Error("This shouldn't be possible")
    }
}

const normalCards = "23456789TJQKA"
const comparableCards = "abcdefghijklm"

function mapComparableCards(cards) {
    return cards.replace(/./g, m => {
        return comparableCards[normalCards.indexOf(m)]
    })
}

function compareHands(a, b) {
    let comparison = a.type - b.type
    if (comparison === 0) {
        comparison = a.comparable.localeCompare(b.comparable)
    }
    return comparison
}

console.log(processfile("input.txt"))