function processfile(filename)
	sum = 0
	for line in eachline(filename)
		println("Checking line " * line)
		game, rest = split(line, ":")
		game = split(game, " ")[2]
		rounds = split(rest, ";")
		if all(valid, rounds)
			println("valid, adding " * game)
			sum += parse(Int64, game)
		end
		println(game)
	end
	return sum
end

function valid(round)
	println("  Checking round " * round)
	parts = split(round, ",")
	totals = Dict("red"=>0, "blue"=>0, "green"=>0)
	for part in parts
		println("    Checking part " * part)
		n, color = split(strip(part), " ")
		totals[color] += parse(Int64, n)
	end
	println(totals)
	totals["red"] <= 12 &&
	totals["green"] <= 13 &&
	totals["blue"] <= 14
end

println(processfile("input.txt"))
