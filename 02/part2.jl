function processfile(filename)
	sum = 0
	for line in eachline(filename)
		println("Checking line " * line)
		game, rest = split(line, ":")
		game = split(game, " ")[2]
		rounds = split(rest, ";")
		println(game)
		sum += gamepower(rounds)
	end
	sum
end

function gamepower(rounds)
	totals = Dict("red"=>0,"green"=>0,"blue"=>0)
	for round in rounds
		hands = split(round, ",")
		for hand in hands
			n, color = split(strip(hand), " ")
			n = parse(Int64, n)
			totals[color] = max(totals[color], n)
		end
	end
	return totals["red"] * totals["green"] * totals["blue"]
end
println(processfile("input.txt"))
