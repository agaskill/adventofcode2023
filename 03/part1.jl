function processfile(filename)
	sum = 0
	lines = readlines(filename)
	for i in 1:length(lines)
		sum += checkline(lines, i)
	end
	sum
end

function checkline(lines, linenum)
	sum = 0
	line = lines[linenum]
	println("Checking line " * line)
	pos = 1
	maxpos = length(line)
	while pos <= maxpos
		if isnumeric(line[pos])
			numstart = pos
			while pos <= maxpos && isnumeric(line[pos])
				pos += 1
			end
			numend = pos - 1
			numvalue = parse(Int64, line[numstart:numend])
			println("Found ", numvalue, " from ", numstart, " to ", numend)
			if issymboladjacent(lines, linenum, numstart, numend)
				println("symbol is adjacent, adding to sum")
				sum += numvalue
			end
		else
			pos += 1
		end
	end
	sum
end

function issymboladjacent(lines, linenum, numstart, numend)
	line = lines[linenum]
	left = max(1, numstart - 1)
	right = min(length(line), numend + 1)

	# check above
	(linenum > 1 && containssymbol(lines[linenum - 1][left:right])) ||
	containssymbol(lines[linenum][left:right]) ||
	(linenum < length(lines) && containssymbol(lines[linenum + 1][left:right]))
end

function containssymbol(str)
	!all(c -> isnumeric(c) || c == '.', str)
end

processfile("input.txt")
