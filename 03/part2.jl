function processfile(filename)
	gears = Dict()
	lines = readlines(filename)
	for i in 1:length(lines)
		checkline(lines, i, gears)
	end
	println(gears)
	sum([nums[1] * nums[2] for nums in values(gears) if length(nums) == 2])
end

function checkline(lines, linenum, gears)
	line = lines[linenum]
	println("Checking line ", line)
	pos = 1
	maxpos = length(line)
	while pos <= maxpos
		if isnumeric(line[pos])
			numstart = pos
			while pos <= maxpos && isnumeric(line[pos])
				pos += 1
			end
			numend = pos - 1
			numvalue = parse(Int64, line[numstart:numend])
			println("Found ", numvalue, " from ", numstart, " to ", numend)
			checkadjacent(lines, linenum, numstart, numend, gears)
		else
			pos += 1
		end
	end
end

function checkadjacent(lines, linenum, numstart, numend, gears)
	line = lines[linenum]
	numvalue = parse(Int64, line[numstart:numend])
	left = max(1, numstart - 1)
	right = min(length(line), numend + 1)
	if linenum > 1
		checksegment(lines, linenum - 1, left, right, numvalue, gears)
	end
	if linenum < length(lines)
		checksegment(lines, linenum + 1, left, right, numvalue, gears)
	end
	if numstart > left
		checksegment(lines, linenum, left, left, numvalue, gears)
	end
	if numend < right
		checksegment(lines, linenum, right, right, numvalue, gears)
	end
end

function checksegment(lines, linenum, left, right, numvalue, gears)
	line = lines[linenum]
	println("Checking segment ", line[left:right])
	for i in left:right
		if line[i] == '*'
			addnumber(gears, numvalue, (i, linenum))
		end
	end
end

function addnumber(gears, numvalue, pos)
	println("Adding ", numvalue, " to position ", pos)
	if in(pos, keys(gears))
		push!(gears[pos], numvalue)
	else
		gears[pos] = [numvalue]
	end
end

processfile("input.txt")
