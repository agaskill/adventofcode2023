function processfile(filename)
	sum([cardvalue(parsecard(line)) for line in eachline(filename)])
end

function parsecard(line)
	cardnum, rest = split(line, ":")
	winning, actual = split(rest, "|")
	parsenums(winning), parsenums(actual)
end

function parsenums(text)
	map(n -> parse(Int64, n), split(text, " ", keepempty=false))
end

function cardvalue((winningnums, actualnums))
	shared = intersect(winningnums, actualnums)
	matches = length(shared)
	if iszero(matches)
		0
	else
		2^(matches - 1)
	end
end

println(processfile("example-1.txt"))
println(processfile("input.txt"))
