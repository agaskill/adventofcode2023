mutable struct Card
	number
	copycount
	matches
end

function processfile(filename)
	cards = [parsecard(line) for line in eachline(filename)]
	ncards = length(cards)
	for i in 1:ncards
		card = cards[i]
		for j in i + 1 : min(ncards, i + card.matches)
			cards[j].copycount += card.copycount
		end
	end
	sum([card.copycount for card in cards])
end

function parsecard(line)
	cardnum, rest = split(line, ":")
	cardnum = parse(Int64,  split(cardnum, " ", keepempty=false)[2])
	winning, actual = split(rest, "|")
	Card(cardnum, 1, length(intersect(parsenums(winning), parsenums(actual))))
end

function parsenums(text)
	map(n -> parse(Int64, n), split(text, " ", keepempty=false))
end

println(processfile("example-1.txt"))
println(processfile("input.txt"))
