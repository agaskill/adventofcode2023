function processfile(filename)
    sum(process(line) for line in eachline(filename))
end

function process(line)
    nums = map(n -> parse(Int64, n), split(line, ' '))
    reducenums(nums)
    println(nums)
    sum(nums)
end

function reducenums(nums)
    for len in length(nums)-1:-1:1
        for i in 1:len
            nums[i] = nums[i+1] - nums[i]
        end
    end
end

println(processfile(ARGS[1]))