function processfile(filename)
    sum(process(line) for line in eachline(filename))
end

function process(line)
    nums = map(n -> parse(Int64, n), split(line, ' '))
    reducenums(nums)
    println(nums)
    foldr(subtractsum, nums)
end

function subtractsum(num, accum)
    num - accum
end

function reducenums(nums)
    for len in 2:length(nums)
        prev = nums[len-1]
        for i in len:length(nums)
            next = nums[i]
            nums[i] -= prev
            prev = next
        end
    end
end

println(processfile(ARGS[1]))