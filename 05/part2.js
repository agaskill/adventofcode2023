const fs = require("fs")

function processfile(filename) {
    const content = fs.readFileSync(filename)
    const lines = content.toString().split("\n")
    const seeds = parseseeds(lines[0])
    const maps = parsemaps(lines)
    let ranges = seeds
    for (const map of maps) {
        map.sort((a, b) => a.from - b.from)
        ranges = ranges.flatMap(input => applymap(input, map))
    }
    return ranges.sort((a, b) => b.start - a.start)
}

// Takes a range and applies a mapping to it, which may split
// it into multiple ranges
function applymap(input, map) {
    //console.log("applying map", map)
    const outputs = []
    let { start, length } = input
    while (length > 0) {
        let outputStart, outputLength
        const entry = map.find(e => e.from <= start && e.from + e.length > start)
        if (entry) {
            // An entry was found that the start is within, produce an output range that is
            // as long as the remaining length in this mapping
            const skip = start - entry.from
            outputStart = entry.to + skip
            outputLength = Math.min(length, entry.length - skip)
            
        }
        else {
            // No mapping, so find the next mapping range above the input
            // (assumes sorted mappings)
            outputStart = start
            const nextEntry = map.find(e => e.from > start)
            if (nextEntry) {
                outputLength = Math.min(length, nextEntry.start - start)
            }
            else {
                // No mapping above, so length is just remaining length
                outputLength = length
            }
        }
        outputs.push({
            start: outputStart,
            length: outputLength
        })
        length -= outputLength
        start += outputLength
    }
    return outputs
}

function parseseeds(line) {
    if (line.startsWith("seeds: ")) {
        const nums = line.substring(7).split(' ').map(Number)
        const ranges = []
        for (let i = 0; i < nums.length; i += 2) {
            ranges.push({start: nums[i], length: nums[i+1]})
        }
        return ranges
    }
    throw new Error("First line should start with seeds:")
}

function parsemaps(lines) {
    const maps = []
    let map
    for (let i = 1; i < lines.length; i++) {
        const line = lines[i]
        if (line.length === 0) {
            map = []
            maps.push(map)
            console.log(lines[++i])
        } else {
            const nums = line.split(' ').map(Number)
            map.push({
                to: nums[0],
                from: nums[1],
                length: nums[2],
            })
        }
    }
    return maps
}

const results = processfile("input.txt")
console.log(results)