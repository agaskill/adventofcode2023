const fs = require("fs")

function processfile(filename) {
    const content = fs.readFileSync(filename)
    const lines = content.toString().split("\n")
    const seeds = parseseeds(lines[0])
    const maps = parsemaps(lines)
    console.log(seeds)
    console.log(maps)
    return seeds.map(function (seed) {
        const location = maps.reduce(applymap, seed)
        console.log(`Seed ${seed} at location ${location}`)
        return {seed, location}
    })
}

function applymap(input, map) {
    //console.log("applying map", map)
    const entry = map.find(e => e.from <= input && e.from + e.length > input)
    if (entry) {
        const output = entry.to + (input - entry.from)
        console.log(`Mapping ${input} to ${output}`)
        return output
    }
    console.log(`No map for ${input}`)
    return input
}

function parseseeds(line) {
    if (line.startsWith("seeds: ")) {
        return line.substring(7).split(' ').map(Number)
    }
    throw new Error("First line should start with seeds:")
}

function parsemaps(lines) {
    const maps = []
    let map
    for (let i = 1; i < lines.length; i++) {
        const line = lines[i]
        if (line.length === 0) {
            map = []
            maps.push(map)
            console.log(lines[++i])
        } else {
            const nums = line.split(' ').map(Number)
            map.push({
                to: nums[0],
                from: nums[1],
                length: nums[2],
            })
        }
    }
    return maps
}

const results = processfile("input.txt")
console.log(results.reduce(function (least, curr) {
    return curr.location < least.location ?
        curr : least }))