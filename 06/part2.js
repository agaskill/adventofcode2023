const fs = require("fs")

function determineWaysToWin(time, distance) {
    // for what value of holdTime does holdTime * (time - holdTime) = distance ?
    // time * holdTime - holdTime^2 = distance
    // holdTime^2 - time * holdTime = -distance
    // h^2 - th + t^2/4 = t^2/4 - d
    // (h - t/2)^2 = t^2/4 - d
    // h - t/2 = +/-sqrt(t^2/4 - d)
    // h = t/2 +/-sqrt(t^2/4 - d)
    const a = time / 2
    const b = Math.sqrt(time * time / 4 - distance)
    const hlow = a - b
    console.log(`Low way to match: ${hlow}`)
    const hhigh = a + b
    console.log(`High way to match: ${hhigh}`)
    return Math.ceil(hhigh) - Math.floor(hlow + 1)
}

function parsenums(str) {
    return Number(str.split(':')[1].replace(/\s+/g, ''))
}

function processfile(filename) {
    const content = fs.readFileSync(filename).toString()
    const [timeline, distanceline] = content.split('\n')
    if (!timeline.startsWith("Time:") ||
        !distanceline.startsWith("Distance:"))
    {
        throw new Error("Invalid prefixes")
    }
    const time = parsenums(timeline)
    const dist = parsenums(distanceline)
    const waysToWin = determineWaysToWin(time, dist)
    console.log(`For time ${time} and distance ${dist} there are ${waysToWin} ways to win`)
}

processfile("input.txt")