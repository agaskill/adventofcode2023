const fs = require("fs")

function determineWaysToWin(time, distance) {
    // for what value of holdTime does holdTime * (time - holdTime) = distance ?
    // time * holdTime - holdTime^2 = distance
    // holdTime^2 - time * holdTime = -distance
    // h^2 - th + t^2/4 = t^2/4 - d
    // (h - t/2)^2 = t^2/4 - d
    // h - t/2 = +/-sqrt(t^2/4 - d)
    // h = t/2 +/-sqrt(t^2/4 - d)
    const a = time / 2
    const b = Math.sqrt(time * time / 4 - distance)
    const hlow = a - b
    console.log(`Low way to match: ${hlow}`)
    const hhigh = a + b
    console.log(`High way to match: ${hhigh}`)
    return Math.ceil(hhigh) - Math.floor(hlow + 1)
}

function parsenums(str) {
    return str.split(':')[1].trim().split(/\s+/g).map(Number)
}

function processfile(filename) {
    const content = fs.readFileSync(filename).toString()
    const [timeline, distanceline] = content.split('\n')
    if (!timeline.startsWith("Time:") ||
        !distanceline.startsWith("Distance:"))
    {
        throw new Error("Invalid prefixes")
    }
    const times = parsenums(timeline)
    const distances = parsenums(distanceline)
    console.log("Times", times)
    console.log("Distances", distances)
    let result = 1
    for (let i = 0; i < times.length; i++) {
        const time = times[i]
        const dist = distances[i]
        const waysToWin = determineWaysToWin(time, dist)
        console.log(`For time ${time} and distance ${dist} there are ${waysToWin} ways to win`)
        result *= waysToWin
    }
    console.log(result)
}

processfile("input.txt")